@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')

<div class="container">
	<div class="">
@include('user.layouts.partials.sidebar')
<div class="tab-content mb-5">

<div class="tab-pane container active" id="fav">
			  	<div class="container">
			  		<div class="row">
			  			<div class="fav-slider">
						  @forelse($available as $item)

					  		<div class="item">
					  			<div class="favourites text-center m-2 p-4">
					  				<div class="product-img py-2">
					  					<img src="{{$item->shop->avatar}}">
					  				</div>
					  				<h4>$10.0 /each</h4>
					  				<h6>{{$item->shop->categories[0]->name}}</h6>
					  				<p>This Dummy Text, not mean to Read</p>
					  				<div class="fav-bottom">
					  					<a href="{{url('/restaurant/details')}}?name={{$item->shop->name}}" class="btn btn-green">Order</a>
					  				</div>
					  			</div>
					  		</div>
					  	 <!-- Restaurant List Box Starts -->
						@empty
                    	<p>No Favourite !</p>
                        @endforelse
                        <!-- Restaurant List Box Starts -->
					  	</div>
					 </div>
			  	</div>
			  </div>
			  </div>
			  </div>
			  </div>
@endsection
