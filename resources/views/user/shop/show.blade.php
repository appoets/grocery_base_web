@extends('user.layouts.app')
@section('content')
<section class="profile-baner">
    <div class="container">
        <div class="row align-items pt-3">
            <div class="col-md-4">
                <div class="store-item text-center">
                    <img src="{{$Shop->avatar}}" class="img-fluid">
                    <div class="p-inner">
                        <h5>{{$Shop->name}}</h5>
                        <div class="cat">{{$Shop->maps_address}}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <form action="/product/search" method="get" class="expanding-search-form">
                    <div class="search-dropdown">
                        <a class="button dropdown-toggle" type="button">
                            <span class="toggle-active">Category</span>
                            <span class="ion-arrow-down-b"></span>
                        </a>
                       <!--  <select class="dropdown-menu" name="cat" required>
                          <option value="Category">  <li><a href="#">Category</a></li> </option>
                           <option value="Produce"> <li><a href="#">Produce</a></li> </option>
                          <option value="Alchocol">  <li><a href="#">Alchocol</a></li> </option>
                           <option value="Fruits"> <li><a href="#">Fruits</a></li> </option>
                        </select> -->


                        <select class="dropdown-menu" name="cat" required>
                                <option>Category</option>
                             @foreach($all_category as $cate)
                                 <option value="{{$cate->id}}">{{$cate->name}}</option>
                             @endforeach
                        </select>

                    </div>
                    <input class="search-input" name="shop" value="{{$Shop->id}}" type="hidden">
                    <input class="search-input" name="prodname"  id="global-search" type="text"  placeholder="Search">
                   <!--  <label class="search-label" for="global-search">
                        <span class="sr-only">Global Search</span>
                    </label>
                    <button class="button search-button" type="submit">
                        <span class="fa fa-search">
                        </span>
                    </button> -->
                     <button  type="submit"><span class="fa fa-search"></span></button>
                </form>
            </div>
        </div>
    </div>
</section>
<div class="container baner-tab">
    <div class="row has-botom-border pt-2 align-items">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tabs-home" role="tab">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-dept" role="tab">Department</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-coupon" role="tab">Coupons</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-items" role="tab">Your Items</a>
            </li>
        </ul>
        <p class="ml-auto">Delivery to <span class="text-green">9001</span> <span class="pl-3">Today, 11AM -
                11PM</span><span class="info-icon">&#x1F6C8;</span></p>
    </div>
</div>

<section>
    <div class="tab-content">
        @forelse($category_list as $key=>$img)

        <div class="tab-pane active" id="tabs-home" role="tabpanel">
            <div class="container">
                <div class="product-outer">
                    <div class="carousel-top">
                        <div class="row align-items carousel-head">
                            <div class="col-6">
                                <h6><b>{{$img->name}}</b></h6>
                            </div>
                            <div class="col-6">
                                <div class=" d-flex align-items float-right">
                                    <ul id="tabs" class="nav nav-tabs">
                                        <li class="nav-item"><a href="" data-target="#home1" data-toggle="tab"
                                                class="nav-link small text-uppercase active">All</a></li>
                                        <li class="nav-item"><a href="" data-target="#profile1" data-toggle="tab"
                                                class="nav-link small text-uppercase ">Sale</a></li>
                                        <li class="nav-item"><a href="" data-target="#messages1" data-toggle="tab"
                                                class="nav-link small text-uppercase">Popular</a></li>
                                        <li class="nav-item"><a href="" data-target="#trends1" data-toggle="tab"
                                                class="nav-link small text-uppercase">Trend</a></li>
                                    </ul>
                                    <a href="#" class="view-link">View More<i class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="product-carousel">
                        <div id="tabsContent" class="tab-content">
                            <div id="home1" class="tab-pane fade active show">
                                <div class="owl-carousel owl-theme">
                                    @forelse(@$img->products as $key=>$products )
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                @forelse(@$products->featured_images as $key=>$pro_image)

                                                <div class="overlay">
                                                    <?php  $img->id ?>
                                                    <h6 class="overlay-btn"><a href="#" class="product_details"
                                                            data-toggle="modal" data-name="{{@$products->name}}"
                                                            data-price="{{currencydecimal(@$products->prices->orignal_price)}}/each"
                                                            data-description="{{@$products->description}}"
                                                            data-img="{{@$pro_image->url}}" ,
                                                            data-shop_id="{{@$products->shop_id}}",
                                                            data-product_id="{{@$products->id}}",
                                                            data-pro_cart_price="{{(@$products->prices->orignal_price)}}",

                                                            data-directions="{{@$products->directions}}",
                                                            data-warnings="{{@$products->warnings}}",
                                                            data-details="{{@$products->details}}",
                                                            data-ingredients="{{@$products->ingredients}}",
                                                            data-view_more="{{@$products->id}}",
                                                            data-related_product="{{@$products->id}}",
                                                            data-category="{{@$img->id}}",
                                                            data-target="#myModal">View</a></h6>
                                                </div>

                                                <div class="product-img py-2">
                                                    <img src="{{@$pro_image->url}}">
                                                </div>
                                                @empty
                                                @endforelse
                                                <h5>{{currencydecimal(@$products->prices->orignal_price)}}</h5>
                                                <h6>{{@$products->name}}</h6>
                                                <p>{{@$products->description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                            <!-- <div id="profile1" class="tab-pane fade">
                                <div class="owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                <div class="overlay">
                                                    <h6 class="overlay-btn"><a href="#" data-toggle="modal"
                                                            data-target="#myModal">View</a></h6>
                                                </div>
                                                <div class="product-img py-2">
                                                    <img src="assets/images/fav1.png">
                                                </div>
                                                <h5>$10.0 /each</h5>
                                                <h6>Apple</h6>
                                                <p>This Dummy Text, not mean to Read</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div> -->
                            <!-- <div id="messages1" class="tab-pane fade">
                                <div class="owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                <div class="overlay">
                                                    <h6 class="overlay-btn"><a href="#" data-toggle="modal"
                                                            data-target="#myModal">View</a></h6>
                                                </div>
                                                <div class="product-img py-2">
                                                    <img src="assets/images/fav1.png">
                                                </div>
                                                <h5>$10.0 /each</h5>
                                                <h6>Apple</h6>
                                                <p>This Dummy Text, not mean to Read</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="trends1" class="tab-pane fade">
                                <div class="owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                <div class="overlay">
                                                    <h6 class="overlay-btn"><a href="#" data-toggle="modal"
                                                            data-target="#myModal">View</a></h6>
                                                </div>
                                                <div class="product-img py-2">
                                                    <img src="assets/images/fav1.png">
                                                </div>
                                                <h5>$10.0 /each</h5>
                                                <h6>Apple</h6>
                                                <p>This Dummy Text, not mean to Read</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div> -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @empty
        @endforelse
        <div class="tab-pane active" id="tabs-dept" role="tabpanel">
</section>


<!-- Modal Popup -->
<div class="modal" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body  pt-5">
                <div id="default" class="padding-top0">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="simple-gallery">

                                <img class="maxi" src="#" id="product_image">

                               <div class="mini" id="product_image_dimension">
                                <!-- <img src="#" id="product_image_dimension"> -->
                                    <!--
                                    <img src="assets/images/fav1.png">
                                    <img src="assets/images/fav1.png">
                                    <img src="assets/images/fav1.png"> -->
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="product-discription">
                                <h5 id="product_name"></h5>
                                <h6 class="text-green product_price"></h6>
                                <p id="product_decription"></p>
                            </div>
                            <div class="d-flex">
                            <form  action="{{Auth::guest()?url('mycart'):url('addcart')}}" method="POST">
                                    {{csrf_field()}}
                                        <div class="value-button" id="decrease" onclick="decreaseValue()"
                                            value="Decrease Value">-</div>
                                        <input type="number" name="quantity" id="number" name= value="1" />
                                        <div class="value-button" id="increase" onclick="increaseValue()"
                                            value="Increase Value">+</div>


                               
                                    <!-- <label>Select Quantity</label> -->
                                    <input type="hidden" id="shop_id"value="" name="shop_id">
                                    <input type="hidden" id="pro_id" value="" name="product_id">
                                    <!-- <input type="hidden" value="1" name="quantity" class="form-control" placeholder="Enter Quantity" readonly min="1" max="100"> -->
                                    <input type="hidden" id="pro_name" value="" name="name">
                                    <input type="hidden" id="pro_price" value="" name="price" />

                                   
                                     <!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->
                                     <button class="cart-btn" type="submit" >Add to Cart <i class="fa fa-shopping-cart"></i></button>

                                 
                                    <!-- <a href="#" class="login-item add-btn"
                                        onclick="$('#login-sidebar').asidebar('open')">@lang('user.add_to_cart')</a> -->

                                    <!-- <a href="#" class="login-item add-btn" data-toggle="modal" data-target="#signin1">@lang('user.add_to_cart')</a> -->

                                </form>

                                <!-- <a class="fav-btn" href="#"><i class="fa fa-heart"></i></a> -->

                               
                                    <!-- <div class="fav-icon">
                                        <div class="icon-wishlist">
                                       
                                        </div>
                                    </div> -->
                                    @if(Auth::guest())

                                    @else
                                    <div class="fav-icon shopfav ">
                                        @if(\App\Favorite::where('shop_id',$Shop->id)->where('user_id',Auth::user()->id)->count()>0)
                                            <div class="icon-wishlist"></div>
                                        @else
                                        <div class="icon-wishlist"></div>
                                        @endif
                                    </div>
                                    @endif
                                    
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tabs-1"
                                            role="tab">Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Ingrediants</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Directions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">Warnings</a>
                                    </li>
                                </ul><!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                        <p id="product_detail"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                                        <p id="product_ingredient"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                                        <p id="product_direction"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-4" role="tabpanel">
                                        <p id="product_warning"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class=" has-botom-border d-flex">
                                <h6><b>Releated Products</b></h6>
                                <p class="ml-auto">
                                    <a href="/restaurant/details?name={{$Shop->name}}" class="view-link">View More<i class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>

                        <div class="row search-content p-3" id="related_pro">
                            
                           <!--  <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Popup -->
    @endsection


    @section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').on('show.bs.modal', function (e) {

                var product_name = $(e.relatedTarget).attr('data-name');
                var product_price = $(e.relatedTarget).attr('data-price');
                var product_decription = $(e.relatedTarget).attr('data-description');
                var img = $(e.relatedTarget).data('img');
                var shop_id = $(e.relatedTarget).attr('data-shop_id');
                var product_id = $(e.relatedTarget).attr('data-product_id');
                var product_cart_price = $(e.relatedTarget).attr('data-pro_cart_price');

                var product_details = $(e.relatedTarget).attr('data-details');
                var product_directions = $(e.relatedTarget).attr('data-directions');
                var product_warnings= $(e.relatedTarget).attr('data-warnings');
                var product_ingredients = $(e.relatedTarget).attr('data-ingredients');

                
                $(this).find('#product_name').text(product_name);
                $(this).find('.product_price').text(product_price);
                $(this).find('#product_decription').text(product_decription);
                $("#product_image").attr("src", img);

                $(this).find('#product_detail').text(product_details);
                $(this).find('#product_direction').text(product_directions);
                $(this).find('#product_warning').text(product_warnings);
                $(this).find('#product_ingredient').text(product_ingredients);

                //addcart
                $("#pro_name").val( product_name );
                $("#pro_price").val( product_cart_price );
                $("#shop_id").val( shop_id );
                $("#pro_id").val( product_id );

                var rel= '';

                var pro_dimension= '';

                var product_cat = $(e.relatedTarget).attr('data-category');

                $.ajax({
                      url: "/related/product",
                      type: "get",
                      data: { 
                        shop_id: shop_id, 
                        category_id: product_cat, 
                      },
                      success: function(response) {
                        console.log(response);
                    $.each( response , function( key, value ) { 

                    $.each( value.products , function( src, dst ) { 


                    rel += '<div class="col-sm-3">\
                                <div class="item">';
                    $.each( dst.featured_images , function( pro, img ) { 

                        rel +=   '<img src='+img.url+' class="img-fluid">';
                    });

                    rel +=   '<div class="p-inner">\
                                            <h5>'+dst.prices.orignal_price+'</h5>\
                                            <h6>'+dst.name+'</h6>\
                                            <div class="cat">'+dst.description+'</div>\
                                        </div>\
                                </div>\
                            </div>';
                   
                    });
                });
                    $('#related_pro').html(rel);
                      },
                      error: function(xhr) {
                        //Do Something to handle error
                      }
                });
                //product image dimension
                $.ajax({
                      url: "/product/imageDimension",
                      type: "get",
                      data: { 
                        shop_id: shop_id, 
                        category_id: product_cat, 
                        product_id: product_id
                    },
                      success: function(response) {
                        console.log(response);
                        $.each( response , function( key, value ) { 

                            $.each( value.products , function( src, dst ) { 

                                $.each( dst.images , function( pro, img ) { 

                                 
                                        console.log(img.url);

                                    
                                        pro_dimension += '<img src='+img.url+'>';
                                        // $("#product_image_dimension").attr("src", img.url);


                    
                                });
                            });
                        });
                        $('#product_image_dimension').html(pro_dimension);

                    },
                    error: function(xhr) {
                        //Do Something to handle error
                    }
                });





            });
        });


       //   $('.search-button').on('click', function(e) {
       //     e.preventDefault(); 
       //    alert('9');
       //     var prod_search = $("#pro_value").val();
       //     alert(prod_search);
       //     $.ajax({
       //         type: "get",
       //         url: '/product/search',
       //         data: {prodname:prod_search},
       //         success: function( msg ) {
       //             alert( msg );
       //         }
       //     });
       // });
       $('.shopfav').on('click',function(){
    if($(".shopfav i" ).hasClass( "active")){
        var url = "{{url('favourite')}}/{{$Shop->id}}";
        var method = 'DELETE';
    }else{
        var url = "{{url('favourite')}}";
        var method = 'POST';
    }
    $.ajax({
        url: url,
        type:method,
        data:{'shop_id':{{$Shop->id}},'_token':"{{csrf_token()}}"},
        success: function(res) { 
            if(method=='POST'){
                $('.shopfav i').addClass('active');
                $('.shopfav i').removeClass('ion-ios-heart-outline');
                $('.shopfav i').addClass('ion-ios-heart');
                $('.fav').html("Favourited");
            }else{
                $('.shopfav i').removeClass('active');
                $('.shopfav i').addClass('ion-ios-heart-outline');
                $('.shopfav i').removeClass('ion-ios-heart');
                $('.fav').html("Favourite");
            }
        },
        error:function(jqXhr,status){ 
            if( jqXhr.status === 422 ) {
                $(".print-error-msg").show();
                var errors = jqXhr.responseJSON; 

                $.each( errors , function( key, value ) { 
                    $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                });
            } 
        }
    });
});
</script>
@endsection